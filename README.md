
This repository is a fork of the <a href="https://github.com/albertomontesg/davis-interactive" target="_blank">DAVIS Interactive Evaluation Framework</a>

The code was modified to support the training of models against the DAVIS Interactive benchmark.

Anonymous repository for a dependency of the main software component proposed in "Solving interactive video object segmentation with label-propagating neural networks", a paper under review for IJCNN 2024. This component is responsible for providing the DAVIS Interactive VOS benchmark and its simulated annotation sessions.
